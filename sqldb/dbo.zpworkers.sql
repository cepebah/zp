﻿CREATE TABLE [dbo].[zpworkers] (
    [Id]                                INT           IDENTITY (1, 1) NOT NULL,
    [Таб. №]                          NVARCHAR (50) NOT NULL,
    [Работник]                         NVARCHAR (50) NOT NULL,
    [Удержание]                       NVARCHAR (50) NOT NULL,
    [Дата начала]                    NVARCHAR (50) NOT NULL,
    [Дата окончания] NVARCHAR (50) NOT NULL,
    [Показатели для расчета начисления]                         NVARCHAR (50) NULL,
    [Результат] NVARCHAR(50) NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

