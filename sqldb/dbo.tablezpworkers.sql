﻿CREATE TABLE [dbo].[zpworkers] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [zpfio]   NVARCHAR (50) NOT NULL,
    [zpwh]    NVARCHAR (50) NOT NULL,
    [zpdata1] NVARCHAR (50) NOT NULL,
    [zpdata2] NVARCHAR (50) NOT NULL,
    [zpmarks] NVARCHAR (50) NULL,
    [zptotal] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

