﻿CREATE TABLE [dbo].[workers]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [N] NVARCHAR(50) NULL, 
    [fio] NVARCHAR(50) NULL, 
    [workshop] NVARCHAR(50) NULL
)
