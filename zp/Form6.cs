﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace zp
{
    public partial class Form6 : Form
    {
        SqlConnection sqlConnection = null;
        //BackgroundWorker worker_a;

        public Form6(SqlConnection connection)
        {
            InitializeComponent();
            //worker_a = new BackgroundWorker();
            //worker_a.RunWorkerCompleted += Worker_a_RunWorkerCompleted;
            //worker_a.DoWork += Worker_a_DoWork;
            //worker_a.ProgressChanged += Worker_a_ProgressChanged;
            //worker_a.WorkerReportsProgress = true;
            sqlConnection = connection;
        }

        private void Worker_a_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
           
        }

        private void Worker_a_DoWork(object sender, DoWorkEventArgs e)
        {
            //e.Result = "OK";
        }

        private void Worker_a_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            /*
            xlApp = new Excel.Application();

            try
            {
                //добавляем книгу
                xlApp.Workbooks.Add(Type.Missing);

                //делаем временно неактивным документ
                xlApp.Interactive = false;
                xlApp.EnableEvents = false;

                //выбираем лист на котором будем работать (Лист 1)
                xlSheet = (Excel.Worksheet)xlApp.Sheets[1];
                //Название листа
                xlSheet.Name = "Данные";

                //Выгрузка данных
                DataTable dt = GetData();

                int collInd = 0;
                int rowInd = 0;
                string data = "";
                
                //называем колонки
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    data = dt.Columns[i].ColumnName.ToString();
                    xlSheet.Cells[1, i + 1] = data;

                    //выделяем первую строку
                    xlSheetRange = xlSheet.get_Range("A1:Z1", Type.Missing);

                    //делаем полужирный текст и перенос слов
                    xlSheetRange.WrapText = true;
                    xlSheetRange.Font.Bold = true;
                }
                
                //заполняем строки
                for (rowInd = 0; rowInd < dt.Rows.Count; rowInd++)
                {
                    for (collInd = 0; collInd < dt.Columns.Count; collInd++)
                    {
                        data = dt.Rows[rowInd].ItemArray[collInd].ToString();
                        xlSheet.Cells[rowInd + 2, collInd + 1] = data;
                    }
                }

                //выбираем всю область данных
                xlSheetRange = xlSheet.UsedRange;

                //выравниваем строки и колонки по их содержимому
                xlSheetRange.Columns.AutoFit();
                xlSheetRange.Rows.AutoFit();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                //Показываем ексель
                xlApp.Visible = true;

                xlApp.Interactive = true;
                xlApp.ScreenUpdating = true;
                xlApp.UserControl = true;

                //Отсоединяемся от Excel
                releaseObject(xlSheetRange);
                releaseObject(xlSheet);
                releaseObject(xlApp);
            }
            */
        }

        //Экземпляр приложения Excel
        Excel.Application xlApp;
        //Лист
        Excel.Worksheet xlSheet;
        //Выделеная область
        Excel.Range xlSheetRange;

        private async void Form6_Load(object sender, EventArgs e)
        {
            //Очищаем временную таблицу от данных
            DelStat();

            // загрузка списка Таб.№ в список comboBox3
            SqlCommand getfio = new SqlCommand("SELECT [N] FROM [workers] ORDER by [N] ASC", sqlConnection);

            SqlDataReader sqlReader = null;

            try
            {
                sqlReader = await getfio.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    comboBox1.Items.Add(sqlReader["N"]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }

            //установка начальной даты
            DateTime dy = new DateTime();
            dy = DateTime.Now;
            
            string dateyear = "01." + dy.Month + "." + dy.Year; // 01 - день, dy.Month - текущий месяц, dy.Year - текущий год

            dateTimePicker1.Value = Convert.ToDateTime(dateyear);

            //фокус на поле для ввода начала периода
            dateTimePicker1.Focus();

            toolStripStatusLabel1.Text = "Количество записей: 0";

            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.View = View.Details;
            listView1.Columns.Add("№");
            listView1.Columns.Add("Таб. №");
            listView1.Columns.Add("Работник");
            listView1.Columns.Add("Удержание");
            listView1.Columns.Add("Дата 'с'");
            listView1.Columns.Add("Дата 'по'");
            listView1.Columns.Add("Код магазина");
            listView1.Columns.Add("Результат");

            autoResizeColumns(listView1);

        }
                
        //авторасширение колонок listView по размеру содержимого
        public static void autoResizeColumns(ListView lv)
        {
            lv.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            ListView.ColumnHeaderCollection cc = lv.Columns;
            for (int i = 0; i < cc.Count; i++)
            {
                int colWidth = TextRenderer.MeasureText(cc[i].Text, lv.Font).Width + 10;
                if (colWidth > cc[i].Width)
                {
                    cc[i].Width = colWidth;
                }
            }
        }

        //закрытие формы и всех открытых соединений
        private void Form6_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
        }

        // Табельный номер
        private async void comboBox1_TextChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            string term = comboBox1.Text;
            var result = from v in comboBox1.Items.Cast<string>() where v.IndexOf(term) >= 0 select v;

            //Показываем строки в которых есть введенное вхождение
            foreach (string find in result)
            {
                comboBox1.AutoCompleteCustomSource.Add(find);
            }

            SqlDataReader sqlReader = null;
            SqlCommand getfio = new SqlCommand("SELECT [N],[fio] FROM [workers]", sqlConnection);                 

            try
            {
                sqlReader = await getfio.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    if (comboBox1.Text == sqlReader["N"].ToString())
                    {
                        textBox1.Text = sqlReader["fio"].ToString();
                    }
                    else if (comboBox1.Text == "")
                    {
                        textBox1.Clear();
                    }
                    else if (textBox1.Text == sqlReader["fio"].ToString() && comboBox1.Text != sqlReader["N"].ToString())
                    {
                        textBox1.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }
        
        private async void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                listView1.Items.Clear();

                SqlDataReader sqlReader = null;

                string query = "";

                string Date1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");

                string Date2 = dateTimePicker2.Value.ToString("yyyy.MM.dd");

                if (comboBox2.Text == "Общий")
                {
                    if (comboBox1.Text == "")
                    {                        
                        query = "SELECT * FROM [zpworkers] WHERE ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio, zpd1 ASC"; //[nzpwh] = '1' OR [nzpwh] = '2' AND

                        dateTimePicker1.Focus();
                    }
                    else
                    {
                        query = "SELECT * FROM [zpworkers] WHERE [N]='" + comboBox1.Text + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1 ASC"; // AND ([nzpwh] >= '1' AND [nzpwh] <= '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio";
                    }
                }
                else if (comboBox2.Text == "Магазин")
                {
                    if (comboBox1.Text == "")
                    {
                        query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio, zpd1 ASC";
                    }
                    else
                    {
                        query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='1' AND [N]='" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1 ASC";
                    }
                }
                else if (comboBox2.Text == "Столовая")
                {
                    if (comboBox1.Text == "")
                    {
                        query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio, zpd1 ASC";
                    }
                    else
                    {
                        query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='2' AND [N] = '" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1 ASC";
                    }
                }

                SqlCommand getzpworkersDateCommand = new SqlCommand(query, sqlConnection);

                try
                {
                    sqlReader = await getzpworkersDateCommand.ExecuteReaderAsync();

                    double S = 0;

                    while (await sqlReader.ReadAsync())
                    {
                        DateTime thisDate1 = new DateTime();
                        thisDate1 = Convert.ToDateTime(sqlReader["zpd1"]);

                        DateTime thisDate2 = new DateTime();
                        thisDate2 = Convert.ToDateTime(sqlReader["zpd2"]);

                        int s = Convert.ToInt32(sqlReader["Id"]);

                        S += Convert.ToDouble(sqlReader["sum"]);

                        ListViewItem item = new ListViewItem(new string[] {

                            //Convert.ToString(sqlReader["Id"]),
                            Convert.ToString(s),
                            Convert.ToString(sqlReader["N"]),
                            Convert.ToString(sqlReader["fio"]),
                            Convert.ToString(sqlReader["zpwh"]),
                            Convert.ToString(thisDate1.ToString("dd.MM.yyyy")),
                            Convert.ToString(thisDate2.ToString("dd.MM.yyyy")),
                            Convert.ToString(sqlReader["rates"]),
                            Convert.ToString(sqlReader["sum"])

                        });

                        listView1.Items.Add(item);
                    }

                    toolStripStatusLabel1.Text = "Итого: " + Convert.ToString(S) + " рублей." + "          " + "Количество записей: " + listView1.Items.Count;

                    autoResizeColumns(listView1);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sqlReader != null && !sqlReader.IsClosed)
                    {
                        sqlReader.Close();
                    }
                }
            }
        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker2.Focus();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void dateTimePicker2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox2.Focus();
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox1.Focus();
            }
        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            comboBox1.Text = "";
        }

        // ИТОГОВАЯ СУММА по человеку/людям
        private /*async*/ void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            string que = "";
            string res = "";

            string Date1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");
            string Date2 = dateTimePicker2.Value.ToString("yyyy.MM.dd");

            //////////////////////////////
            ///
            ///     ОБЩАЯ СУММА ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            if (comboBox2.Text == "Общий")
            {
                if (comboBox1.Text == "")
                {
                    string id = "";
                    int n = 1;

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            toolStripStatusLabel1.Text = "Подождите... Идёт вычисление.";

                            ListViewItem item = new ListViewItem(new string[] {

                                //Convert.ToString(sqlReader["Id"]),
                                Convert.ToString(n),
                                // Таб №
                                Convert.ToString(id),
                                // ФИО
                                Convert.ToString(fio),
                                // Удержание
                                Convert.ToString("За питание и товары в счёт зар. платы"),
                                // Дата с
                                Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                                // Дата по
                                Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                                // Показатель
                                Convert.ToString("Сумма"),
                                // Результат
                                Convert.ToString(result)

                            });

                            n++;
                            listView1.Items.Add(item);

                            try
                            {
                                quesumCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    autoResizeColumns(listView1);

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Итого: " + res + " рублей в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + "          " + "Количество записей: " + listView1.Items.Count;

                }
                else
                {
                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + comboBox1.Text + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    ListViewItem item = new ListViewItem(new string[] {

                        //Convert.ToString(sqlReader["Id"]),
                        Convert.ToString("1"),
                        // Таб №
                        Convert.ToString(comboBox1.Text),
                        // ФИО
                        Convert.ToString(textBox1.Text),
                        // Удержание
                        Convert.ToString("За питание и товары в счёт зар. платы"),
                        // Дата с
                        Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                        // Дата по
                        Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                        // Показатель
                        Convert.ToString("Сумма"),
                        // Результат
                        Convert.ToString(res)

                    });

                    listView1.Items.Add(item);

                    autoResizeColumns(listView1);

                    toolStripStatusLabel1.Text = "Итого: " + textBox1.Text + " -- " + res + " рублей (в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + ") " + "          " + "Количество записей: " + listView1.Items.Count;

                }
            }

            //////////////////////////////
            ///
            ///     МАГАЗИН ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            else if (comboBox2.Text == "Магазин")
            {
                if (comboBox1.Text == "")
                {
                    string id = "";
                    int n = 1;

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND [nzpwh] = '1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            toolStripStatusLabel1.Text = "Подождите... Идёт вычисление.";

                            ListViewItem item = new ListViewItem(new string[] {

                                //Convert.ToString(sqlReader["Id"]),
                                Convert.ToString(n),
                                // Таб №
                                Convert.ToString(id),
                                // ФИО
                                Convert.ToString(fio),
                                // Удержание
                                Convert.ToString("За питание и товары в счёт зар. платы"),
                                // Дата с
                                Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                                // Дата по
                                Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                                // Показатель
                                Convert.ToString("Сумма"),
                                // Результат
                                Convert.ToString(result)

                            });

                            n++;
                            listView1.Items.Add(item);

                            try
                            {
                                quesumCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    autoResizeColumns(listView1);

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Итого: " + res + " рублей в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + "          " + "Количество записей: " + listView1.Items.Count;

                }
                else
                {
                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='1' AND [N]='" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    ListViewItem item = new ListViewItem(new string[] {

                        //Convert.ToString(sqlReader["Id"]),
                        Convert.ToString("1"),
                        // Таб №
                        Convert.ToString(comboBox1.Text),
                        // ФИО
                        Convert.ToString(textBox1.Text),
                        // Удержание
                        Convert.ToString("За питание и товары в счёт зар. платы"),
                        // Дата с
                        Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                        // Дата по
                        Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                        // Показатель
                        Convert.ToString("Сумма"),
                        // Результат
                        Convert.ToString(res)

                    });

                    listView1.Items.Add(item);

                    autoResizeColumns(listView1);

                    toolStripStatusLabel1.Text = "Итого по Магазину: " + textBox1.Text + " -- " + res + " рублей (в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + ") " + "          " + "Количество записей: " + listView1.Items.Count;
                }
            }

            //////////////////////////////
            ///
            ///     СТОЛОВАЯ ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            else if (comboBox2.Text == "Столовая")
            {
                if (comboBox1.Text == "")
                {
                    string id = "";
                    int n = 1;

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND [nzpwh] = '2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            toolStripStatusLabel1.Text = "Подождите... Идёт вычисление.";

                            ListViewItem item = new ListViewItem(new string[] {

                                //Convert.ToString(sqlReader["Id"]),
                                Convert.ToString(n),
                                // Таб №
                                Convert.ToString(id),
                                // ФИО
                                Convert.ToString(fio),
                                // Удержание
                                Convert.ToString("За питание и товары в счёт зар. платы"),
                                // Дата с
                                Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                                // Дата по
                                Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                                // Показатель
                                Convert.ToString("Сумма"),
                                // Результат
                                Convert.ToString(result)

                            });

                            n++;
                            listView1.Items.Add(item);

                            try
                            {
                                quesumCommand2.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    autoResizeColumns(listView1);

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Итого: " + res + " рублей в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + "          " + "Количество записей: " + listView1.Items.Count;

                }
                else
                {
                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='2' AND [N] = '" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    ListViewItem item = new ListViewItem(new string[] {

                        //Convert.ToString(sqlReader["Id"]),
                        Convert.ToString("1"),
                        // Таб №
                        Convert.ToString(comboBox1.Text),
                        // ФИО
                        Convert.ToString(textBox1.Text),
                        // Удержание
                        Convert.ToString("За питание и товары в счёт зар. платы"),
                        // Дата с
                        Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")),
                        // Дата по
                        Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")),
                        // Показатель
                        Convert.ToString("Сумма"),
                        // Результат
                        Convert.ToString(res)

                    });

                    listView1.Items.Add(item);

                    autoResizeColumns(listView1);

                    toolStripStatusLabel1.Text = "Итого по Столовой: " + textBox1.Text + " -- " + res + " рублей (в период с " + Convert.ToString(dateTimePicker1.Value.ToString("dd.MM.yyyy")) + " по " + Convert.ToString(dateTimePicker2.Value.ToString("dd.MM.yyyy")) + ") " + "          " + "Количество записей: " + listView1.Items.Count;
                    
                }
            }            
        }

        //====================================================================================================
        //====================================================================================================

        private async void totalsum()
        {
            toolStripStatusLabel1.Text = "Формирование таблицы данных...";

            string que = "";
            string res = "";

            string Date1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");
            string Date2 = dateTimePicker2.Value.ToString("yyyy.MM.dd");

            int k = 0;

            //////////////////////////////
            ///
            ///     ОБЩАЯ СУММА ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            if (comboBox2.Text == "Общий")
            {
                // если не введён Таб. №
                if (comboBox1.Text == "")
                {
                    string id = "";

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            // запись данных во временную таблицу БД
                            SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                            insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(id));
                            insertsqlwaitres.Parameters.AddWithValue("fio", fio);

                            string zpwh1 = "За питание и товары в счёт зар. платы";
                            insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh1);

                            // преобразование формата даты в вид 01.01.2001
                            string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                            insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                            string s = "Сумма";
                            insertsqlwaitres.Parameters.AddWithValue("summa", s);

                            insertsqlwaitres.Parameters.AddWithValue("rates", Convert.ToString(comboBox2.Text));

                            insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToString(result));

                            toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                            try
                            {
                                insertsqlwaitres.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            //Считает кол-во записей передаваемых в Excel
                            k++;
                        }                        
                    }

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: " + k;

                }
                else
                {
                    //Очищаем временную таблицу от данных
                    DelStat();

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + comboBox1.Text + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    // запись данных во временную таблицу БД
                    SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                    insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(comboBox1.Text));
                    insertsqlwaitres.Parameters.AddWithValue("fio", Convert.ToString(textBox1.Text));

                    string zpwh = "За питание и товары в счёт зар. платы";
                    insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh);

                    // преобразование формата даты в вид 01.01.2001
                    string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                    insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                    string s = "Сумма";
                    insertsqlwaitres.Parameters.AddWithValue("summa", s);

                    insertsqlwaitres.Parameters.AddWithValue("rates", comboBox2.Text);
                    insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToDouble(res));

                    toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                    try
                    {
                        await insertsqlwaitres.ExecuteNonQueryAsync();                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: 1";
                }
            }

            //////////////////////////////
            ///
            ///     МАГАЗИН ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            else if (comboBox2.Text == "Магазин")
            {
                if (comboBox1.Text == "")
                {
                    string id = "";

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND [nzpwh] = '1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            // запись данных во временную таблицу БД
                            SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                            insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(id));
                            insertsqlwaitres.Parameters.AddWithValue("fio", fio);

                            string zpwh1 = "За питание и товары в счёт зар. платы";
                            insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh1);

                            // преобразование формата даты в вид 01.01.2001
                            string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                            insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                            string s = "Сумма";
                            insertsqlwaitres.Parameters.AddWithValue("summa", s);

                            insertsqlwaitres.Parameters.AddWithValue("rates", Convert.ToString(comboBox2.Text));

                            insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToString(result));

                            toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                            try
                            {
                                insertsqlwaitres.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            //Считает кол-во записей передаваемых в Excel
                            k++;
                        }
                    }

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: " + k;

                }
                else
                {
                    //Очищаем временную таблицу от данных
                    DelStat();

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='1' AND [N]='" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    // запись данных во временную таблицу БД
                    SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                    insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(comboBox1.Text));
                    insertsqlwaitres.Parameters.AddWithValue("fio", Convert.ToString(textBox1.Text));

                    string zpwh = "За питание и товары в счёт зар. платы";
                    insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh);

                    // преобразование формата даты в вид 01.01.2001
                    string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                    insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                    string s = "Сумма";
                    insertsqlwaitres.Parameters.AddWithValue("summa", s);

                    insertsqlwaitres.Parameters.AddWithValue("rates", comboBox2.Text);
                    insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToDouble(res));

                    toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                    try
                    {
                        await insertsqlwaitres.ExecuteNonQueryAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: 1";

                }
            }

            //////////////////////////////
            ///
            ///     СТОЛОВАЯ ДЛЯ ВСЕХ
            ///
            //////////////////////////////

            else if (comboBox2.Text == "Столовая")
            {
                if (comboBox1.Text == "")
                {
                    string id = "";

                    SqlDataReader sqlReader = null;

                    SqlCommand getfio = new SqlCommand("SELECT * FROM [workers]", sqlConnection);

                    sqlReader = getfio.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        id = sqlReader["N"].ToString();
                        string fio = sqlReader["fio"].ToString();

                        string query = "SELECT SUM(sum) FROM [zpworkers] WHERE [N]='" + id + "' AND [nzpwh] = '2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "')";
                        SqlCommand quesumCommand2 = new SqlCommand(query, sqlConnection);
                        string result = Convert.ToString(quesumCommand2.ExecuteScalar());

                        if (result != "")
                        {
                            // запись данных во временную таблицу БД
                            SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                            insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(id));
                            insertsqlwaitres.Parameters.AddWithValue("fio", fio);

                            string zpwh1 = "За питание и товары в счёт зар. платы";
                            insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh1);

                            // преобразование формата даты в вид 01.01.2001
                            string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                            insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                            insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                            string s = "Сумма";
                            insertsqlwaitres.Parameters.AddWithValue("summa", s);

                            insertsqlwaitres.Parameters.AddWithValue("rates", Convert.ToString(comboBox2.Text));

                            insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToString(result));

                            toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                            try
                            {
                                insertsqlwaitres.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            //Считает кол-во записей передаваемых в Excel
                            k++;
                        }
                    }

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: " + k;

                }
                else
                {
                    //Очищаем временную таблицу от данных
                    DelStat();

                    que = "SELECT SUM(sum) FROM [zpworkers] WHERE [nzpwh]='2' AND [N] = '" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ";

                    SqlCommand quesumCommand = new SqlCommand(que, sqlConnection);

                    res = Convert.ToString(quesumCommand.ExecuteScalar());

                    if (res == "")
                    {
                        res = "0";
                    }

                    // запись данных во временную таблицу БД
                    SqlCommand insertsqlwaitres = new SqlCommand("INSERT INTO [zptempwaitres] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum )", sqlConnection);

                    insertsqlwaitres.Parameters.AddWithValue("N", Convert.ToString(comboBox1.Text));
                    insertsqlwaitres.Parameters.AddWithValue("fio", Convert.ToString(textBox1.Text));

                    string zpwh = "За питание и товары в счёт зар. платы";
                    insertsqlwaitres.Parameters.AddWithValue("zpwh", zpwh);

                    // преобразование формата даты в вид 01.01.2001
                    string theDate1 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    string theDate2 = dateTimePicker1.Value.ToString("dd.MM.yyyy");
                    insertsqlwaitres.Parameters.AddWithValue("zpd1", theDate1);
                    insertsqlwaitres.Parameters.AddWithValue("zpd2", theDate2);

                    string s = "Сумма";
                    insertsqlwaitres.Parameters.AddWithValue("summa", s);

                    insertsqlwaitres.Parameters.AddWithValue("rates", comboBox2.Text);
                    insertsqlwaitres.Parameters.AddWithValue("sum", Convert.ToDouble(res));

                    toolStripStatusLabel1.Text = "Запись данных в таблицу...";

                    try
                    {
                        await insertsqlwaitres.ExecuteNonQueryAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    toolStripStatusLabel1.Text = "Количество записей в Excel: 1";

                }
            }
        }

        //====================================================================================================

        private DataTable GetData()
        {
            //строка соединения
            string connString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\Users\User\Desktop\_VS Project\zp\zp\Database1.mdf; Integrated Security = True; MultipleActiveResultSets=True";

            //соединение
            SqlConnection con = new SqlConnection(connString);

            DataTable dt = new DataTable();

            try
            {
                string query = @"SELECT * FROM [zptempwaitres] ORDER BY [fio]";
                SqlCommand comm = new SqlCommand(query, con);

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

            return dt;
        }

        //====================================================================================================

        //////////////////////////////
        ///         ВЫГРУЗКА
        ///            В
        ///          EXCEL
        //////////////////////////////

        private void button2_Click(object sender, EventArgs e)
        {
            totalsum();
           // var r = new List<string>();
           // r.Add("efa");
            //worker_a.RunWorkerAsync(r);
            xlApp = new Excel.Application();

            try
            {
                //добавляем книгу
                xlApp.Workbooks.Add(Type.Missing);

                //делаем временно неактивным документ
                xlApp.Interactive = false;
                xlApp.EnableEvents = false;

                //выбираем лист на котором будем работать (Лист 1)
                xlSheet = (Excel.Worksheet)xlApp.Sheets[1];
                //Название листа
                xlSheet.Name = "Данные";

                //Выгрузка данных
                DataTable dt = GetData();

                int collInd = 0;
                int rowInd = 0;
                string data = "";

                //называем колонки
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    data = dt.Columns[i].ColumnName.ToString();
                    xlSheet.Cells[1, i + 1] = data;

                    //выделяем первую строку
                    xlSheetRange = xlSheet.get_Range("A1:Z1", Type.Missing);

                    //делаем полужирный текст и перенос слов
                    xlSheetRange.WrapText = true;
                    xlSheetRange.Font.Bold = true;
                }

                //заполняем строки
                for (rowInd = 0; rowInd < dt.Rows.Count; rowInd++)
                {
                    for (collInd = 0; collInd < dt.Columns.Count; collInd++)
                    {
                        data = dt.Rows[rowInd].ItemArray[collInd].ToString();
                        xlSheet.Cells[rowInd + 2, collInd + 1] = data;
                    }
                }

                //выбираем всю область данных
                xlSheetRange = xlSheet.UsedRange;

                //выравниваем строки и колонки по их содержимому
                xlSheetRange.Columns.AutoFit();
                xlSheetRange.Rows.AutoFit();

                //Очищаем временную таблицу от данных
                DelStat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                //Показываем ексель
                xlApp.Visible = true;

                xlApp.Interactive = true;
                xlApp.ScreenUpdating = true;
                xlApp.UserControl = true;

                //Отсоединяемся от Excel
                releaseObject(xlSheetRange);
                releaseObject(xlSheet);
                releaseObject(xlApp);

            }
        }        

        //очищение таблицы временной БД
        private async void DelStat()
        {
            // очищение таблицы в БД
            string tempempty = "TRUNCATE TABLE [zptempwaitres]";
            //string tempempty = "DELETE from [zptempwaitres]";

            // соединение
            SqlCommand sqlwaitres = new SqlCommand(tempempty, sqlConnection);

            try
            {
                await sqlwaitres.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //====================================================================================================
        //====================================================================================================

        //Освобождаем ресуры (закрываем Excel)
        void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show(ex.ToString(), "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                GC.Collect();
            }
        }

        // вывод результата по кнопке
        private async void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            SqlDataReader sqlReader = null;

            string query = "";
            string Date1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");
            string Date2 = dateTimePicker2.Value.ToString("yyyy.MM.dd");

            if (comboBox2.Text == "Общий")
            {
                if (comboBox1.Text == "")
                {
                    query = "SELECT * FROM [zpworkers] WHERE ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio";

                    dateTimePicker1.Focus();
                }
                else
                {
                    query = "SELECT * FROM [zpworkers] WHERE [N]='" + comboBox1.Text + "' AND ([nzpwh] BETWEEN '1' AND '2') AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1";
                }
            }
            else if (comboBox2.Text == "Магазин")
            {
                if (comboBox1.Text == "")
                {
                    query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='1' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio";
                }
                else
                {
                    query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='1' AND [N]='" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1";
                }
            }
            else if (comboBox2.Text == "Столовая")
            {
                if (comboBox1.Text == "")
                {
                    query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='2' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY fio";
                }
                else
                {
                    query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='2' AND [N] = '" + comboBox1.Text + "' AND ([zpd1] >= '" + Date1 + "' AND [zpd1] <= '" + Date2 + "') ORDER BY zpd1";
                }
            }

            SqlCommand getzpworkersDateCommand = new SqlCommand(query, sqlConnection);

            try
            {
                sqlReader = await getzpworkersDateCommand.ExecuteReaderAsync();

                double S = 0;

                while (await sqlReader.ReadAsync())
                {
                    DateTime thisDate1 = new DateTime();
                    thisDate1 = Convert.ToDateTime(sqlReader["zpd1"]);

                    DateTime thisDate2 = new DateTime();
                    thisDate2 = Convert.ToDateTime(sqlReader["zpd2"]);

                    int s = Convert.ToInt32(sqlReader["Id"]);

                    S += Convert.ToDouble(sqlReader["sum"]);

                    ListViewItem item = new ListViewItem(new string[] {

                        //Convert.ToString(sqlReader["Id"]),
                        Convert.ToString(s),
                        Convert.ToString(sqlReader["N"]),
                        Convert.ToString(sqlReader["fio"]),
                        Convert.ToString(sqlReader["zpwh"]),
                        Convert.ToString(thisDate1.ToString("dd.MM.yyyy")),
                        //Convert.ToString(sqlReader["zpd1"]),
                        Convert.ToString(thisDate2.ToString("dd.MM.yyyy")),
                        Convert.ToString(sqlReader["rates"]),
                        Convert.ToString(sqlReader["sum"])

                    });

                    listView1.Items.Add(item);
                }

                toolStripStatusLabel1.Text = "Итого: " + Convert.ToString(S) + " рублей." + "          " + "Количество записей: " + listView1.Items.Count;

                autoResizeColumns(listView1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                {
                    sqlReader.Close();
                }
            }
        }
    }
}