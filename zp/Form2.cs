﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions; // для Regex
using MySql.Data.MySqlClient;

namespace zp
{
    public partial class Form2 : Form
    {
        SqlConnection SqlConnection = null;

        public Form2(SqlConnection connection)
        {
            InitializeComponent();

            SqlConnection = connection;
        }

        private async void Form2_Load(object sender, EventArgs e)
        {
            // загрузка списка Таб.№ в список comboBox3

            SqlCommand getfio = new SqlCommand("SELECT [N] FROM [workers]", SqlConnection);

            SqlDataReader sqlReader = null;

            try
            {
                sqlReader = await getfio.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    comboBox3.Items.Add(sqlReader["N"]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }

        // выбор удержания
        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Льготное питание механизаторов")
            {
                comboBox2.Text = "Столовая";                                // выбор кода магазина - Столовая

                //comboBox2.Items.Remove(0);
                comboBox2.Items.RemoveAt(0);
                //comboBox2.Text = "Столовая";

                comboBox3.Focus();
            }
            else if (comboBox1.Text == "За питание и товары в счёт зар. платы")
            {
                comboBox2.Items.Clear();

                comboBox2.Items.Add("Магазин");
                comboBox2.Items.Add("Столовая");
                comboBox2.Text = "Магазин";                                 // выбор кода магазина - Магазин
                comboBox3.Focus();
            }
        }

        // Табельный номер
        private async void comboBox3_TextChanged(object sender, EventArgs e)
        {
            string term = comboBox3.Text;
            var result = from v in comboBox3.Items.Cast<string>() where v.IndexOf(term) >= 0 select v;

            //Показываем строки в которых есть введенное вхождение
            foreach (string find in result)
            {
                comboBox3.AutoCompleteCustomSource.Add(find);
            }

            SqlDataReader sqlReader = null;

            SqlCommand getfio = new SqlCommand("SELECT [N],[fio] FROM [workers]", SqlConnection);

            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            //builder.MultipleActiveResultSets = true;                     

            try
            {
                sqlReader = await getfio.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    if (comboBox3.Text == sqlReader["N"].ToString())
                    {
                        textBox1.Text = sqlReader["fio"].ToString();
                    }
                    else if (comboBox3.Text == "")
                    {
                        textBox1.Clear();
                    }
                    else if (textBox1.Text == sqlReader["fio"].ToString() && comboBox3.Text != sqlReader["N"].ToString())
                    {
                        textBox1.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }

        private void comboBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && comboBox3.Text == "") // проверка на незаполненное поле "Таб.№"
            {
                MessageBox.Show("Не заполнено обязательное поле 'Таб. №'!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.KeyCode == Keys.Enter && comboBox3.Text != "")
            {
                if (e.KeyCode == Keys.Enter && textBox1.Text == "") // проверка на отсутствие табельного номера в базе
                {
                    MessageBox.Show("Такого работника не существует!\nНеобходимо добавить нового работника!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comboBox3.Text = "";
                    comboBox3.Focus();

                }
                else if (textBox1.Text != "")
                {
                    textBox2.Focus();
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private async void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            ///
            /// СОХРАНЕНИЕ ДАННЫХ по нажатию Enter
            ///

            if (e.KeyCode == Keys.Enter && textBox2.Text != "")
            {
                //button1.Focus();

                SqlCommand insertzpworkersCommand = new SqlCommand("INSERT INTO [zpworkers] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum, nzpwh)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum, @nzpwh)", SqlConnection);

                insertzpworkersCommand.Parameters.AddWithValue("N", comboBox3.Text);
                insertzpworkersCommand.Parameters.AddWithValue("fio", textBox1.Text);
                insertzpworkersCommand.Parameters.AddWithValue("sum", Convert.ToDouble(textBox2.Text));


                //преобразование формата даты из обычного в обратный для сортировки
                string theDate1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");
                //string theDate2 = dateTimePicker2.Value.ToString("yyyy.MM.dd"); //вариант добавления элемента с периодом дат

                insertzpworkersCommand.Parameters.AddWithValue("zpd1", theDate1);
                insertzpworkersCommand.Parameters.AddWithValue("zpd2", theDate1);
                //insertzpworkersCommand.Parameters.AddWithValue("zpd2", theDate2);
                insertzpworkersCommand.Parameters.AddWithValue("zpwh", comboBox1.Text);
                insertzpworkersCommand.Parameters.AddWithValue("rates", comboBox2.Text);

                string s = "Сумма";
                insertzpworkersCommand.Parameters.AddWithValue("summa", s);

                string s1 = "1";
                string s2 = "2";
                string s3 = "3";

                if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Магазин")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s1);
                }
                else if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Столовая")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s2);
                }
                else if (comboBox1.Text == "Льготное питание механизаторов" & comboBox2.Text == "Столовая")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s3);
                }

                try
                {
                    await insertzpworkersCommand.ExecuteNonQueryAsync();

                    comboBox3.Text = "";
                    textBox1.Clear();
                    textBox2.Clear();

                    comboBox3.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else if (e.KeyCode == Keys.Enter && textBox2.Text == "")
                MessageBox.Show("Не заполнено обязательное поле 'Сумма'!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        // Кнопка СОХРАНИТЬ
        private async void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                //button1.Focus();

                SqlCommand insertzpworkersCommand = new SqlCommand("INSERT INTO [zpworkers] (N, fio, zpwh, zpd1, zpd2, summa, rates, sum, nzpwh)VALUES(@N, @fio, @zpwh, @zpd1, @zpd2, @summa, @rates, @sum, @nzpwh)", SqlConnection);

                insertzpworkersCommand.Parameters.AddWithValue("N", comboBox3.Text);
                insertzpworkersCommand.Parameters.AddWithValue("fio", textBox1.Text);
                insertzpworkersCommand.Parameters.AddWithValue("sum", Convert.ToDouble(textBox2.Text));

                //преобразование формата даты из обычного в обратный для сортировки
                string theDate1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");
                //string theDate2 = dateTimePicker2.Value.ToString("yyyy.MM.dd"); //вариант добавления элемента с периодом дат

                insertzpworkersCommand.Parameters.AddWithValue("zpd1", theDate1);
                insertzpworkersCommand.Parameters.AddWithValue("zpd2", theDate1);
                //insertzpworkersCommand.Parameters.AddWithValue("zpd2", theDate2);
                insertzpworkersCommand.Parameters.AddWithValue("zpwh", comboBox1.Text);
                insertzpworkersCommand.Parameters.AddWithValue("rates", comboBox2.Text);

                string s = "Сумма";
                insertzpworkersCommand.Parameters.AddWithValue("summa", s);

                string s1 = "1";
                string s2 = "2";
                string s3 = "3";

                if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Магазин")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s1);
                }
                else if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Столовая")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s2);
                }
                else if (comboBox1.Text == "Льготное питание механизаторов" & comboBox2.Text == "Столовая")
                {
                    insertzpworkersCommand.Parameters.AddWithValue("nzpwh", s3);
                }

                try
                {
                    await insertzpworkersCommand.ExecuteNonQueryAsync();

                    comboBox3.Text = "";
                    textBox1.Clear();
                    textBox2.Clear();

                    comboBox3.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            e.Handled = !(char.IsDigit(c) || c == '\b');
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            // конвертирование запятой в точку
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }

            // ограничение ввода
            if (e.KeyChar < '0' | e.KeyChar > '9' && e.KeyChar != (char)Keys.Back && e.KeyChar != ',')
            {
                e.Handled = true;
            }
            
            // запрет ввода разделителя первым символом.
            if (textBox2.SelectionStart == 0 & e.KeyChar == ',')
            {
                e.Handled = true;
            }
            
            // запрет таких значений как 03.xx 09999.xx После 0 должа быть точка. 
            if (textBox2.Text == "0")
            {
                if (e.KeyChar != ',' & e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
            
            // запрет на ввод таких значений как "97.980", "0.33333."
            // Должно быть "8.30", "5.09"
            if (textBox2.Text.IndexOf(',') > 0)
            {
                if (textBox2.Text.Substring(textBox2.Text.IndexOf(',')).Length > 2)
                {
                    if (e.KeyChar != (char)Keys.Back)
                    {
                        e.Handled = true;
                    }
                }
            }
            
            //Ввод только 1 разделителя
            if (e.KeyChar == ',')
            {
                if (textBox2.Text.IndexOf(',') != -1)
                {
                    e.Handled = true;
                }

            }
        }
    }
}
