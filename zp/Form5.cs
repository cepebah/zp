﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zp
{
    public partial class Form5 : Form
    {
        SqlConnection SqlConnection = null;

        int idn;

        public Form5(SqlConnection connection, int idn)
        {
            InitializeComponent();

            SqlConnection = connection;

            this.idn = idn;
        }
        
        private async void Form5_Load(object sender, EventArgs e)
        {
            SqlCommand getUpdateMemberCommand = new SqlCommand("SELECT [N], [fio], [workshop] FROM [workers] WHERE [Id]=@id", SqlConnection);

            SqlDataReader sqlReader = null;

            getUpdateMemberCommand.Parameters.AddWithValue("Id", idn);

            try
            {
                sqlReader = await getUpdateMemberCommand.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    textBox1.Text = Convert.ToString(sqlReader["N"]);
                    textBox2.Text = Convert.ToString(sqlReader["fio"]);
                    comboBox1.Text = Convert.ToString(sqlReader["workshop"]);
                }

                // выделение всего текста в поле
                // для быстрого редактирования
                // после получения фокуса

                textBox1.SelectAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                {
                    sqlReader.Close();
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            SqlCommand updateCommand = new SqlCommand("UPDATE [workers] SET [N]=@N, [fio]=@fio, [workshop]=@workshop WHERE [Id]=@Id", SqlConnection);

            updateCommand.Parameters.AddWithValue("Id", idn);

            updateCommand.Parameters.AddWithValue("N", textBox1.Text);
            updateCommand.Parameters.AddWithValue("fio", textBox2.Text);
            updateCommand.Parameters.AddWithValue("workshop", comboBox1.Text);

            try
            {
                await updateCommand.ExecuteNonQueryAsync();

                MessageBox.Show("Изменения сохранены! \nЧтобы продолжить, нажмите 'ОК'.", "Сохранение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }

        private async void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            ///
            /// загрузка списка comboBox1 из БД по нажатию ENTER в поле textBox2
            ///

            if (e.KeyCode == Keys.Enter)
            {
                SqlCommand getsubdivCommand = new SqlCommand("SELECT [sub] FROM [subdiv]", SqlConnection);

                SqlDataReader sqlReader = null;

                try
                {
                    sqlReader = await getsubdivCommand.ExecuteReaderAsync();

                    while (await sqlReader.ReadAsync())
                    {
                        comboBox1.Items.Add(sqlReader["sub"]);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                finally
                {
                    if (sqlReader != null && !sqlReader.IsClosed)
                        sqlReader.Close();
                }

                comboBox1.Focus();
            }
        }        

        // загрузка списка comboBox1 из БД по нажатию клавиши мыши
        private async void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {
            comboBox1.Items.Clear();

            comboBox1.SelectAll();
            
            SqlCommand getsubdivCommand = new SqlCommand("SELECT [sub] FROM [subdiv]", SqlConnection);

            SqlDataReader sqlReader = null;

            try
            {
                sqlReader = await getsubdivCommand.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    comboBox1.Items.Add(sqlReader["sub"]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }            
        }    

        private async void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            ///
            /// СОХРАНЕНИЕ ДАННЫХ по нажатию Enter
            ///

            if (e.KeyCode == Keys.Enter)
            {
                //button1.Focus();

                SqlCommand updateCommand = new SqlCommand("UPDATE [workers] SET [N]=@N, [fio]=@fio, [workshop]=@workshop WHERE [Id]=@Id", SqlConnection);

                updateCommand.Parameters.AddWithValue("Id", idn);

                updateCommand.Parameters.AddWithValue("N", textBox1.Text);
                updateCommand.Parameters.AddWithValue("fio", textBox2.Text);
                updateCommand.Parameters.AddWithValue("workshop", comboBox1.Text);

                try
                {
                    await updateCommand.ExecuteNonQueryAsync();

                    MessageBox.Show("Изменения сохранены! \nЧтобы продолжить, нажмите 'ОК'.", "Изменение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            string term = comboBox1.Text;
            var result = from v in comboBox1.Items.Cast<string>() where v.IndexOf(term) >= 0 select v;

            //Показываем строки в которых есть введенное вхождение
            foreach (string find in result)
            {
                comboBox1.AutoCompleteCustomSource.Add(find);
            }
        }
    }
}
