﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace zp
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        
        public Form1()
        {
            InitializeComponent();

            this.KeyPreview = true;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                // Горячая клавиша "ДОБАВИТЬ УДЕРЖАНИЕ"
                case Keys.D1:
                    if (e.KeyCode == Keys.D1 && e.Alt)              //  Alt + 1
                    {
                        addwhToolStripMenu1.PerformClick();         //  имитируем нажатие 
                    }
                    break;
                // Горячая клавиша "ДОБАВИТЬ РАБОТНИКА"
                case Keys.D2:
                    if (e.KeyCode == Keys.D2 && e.Alt)              //  Alt + 2
                    {
                        addmemToolStripMenu1.PerformClick();        //  имитируем нажатие 
                    }
                    break;

                // Горячая клавиша "ИЗМЕНИТЬ УДЕРЖАНИЕ"
                case Keys.D3:
                    if (e.KeyCode == Keys.D3 && e.Alt)              //  Alt + 3
                    {
                        editToolStripMenu1.PerformClick();          //  имитируем нажатие 
                    }
                    break;
                // Горячая клавиша "УДАЛИТЬ УДЕРЖАНИЕ"
                case Keys.Delete:
                    if (e.KeyCode == Keys.Delete)                   //  Del
                    {
                        deltoolStripMenu1.PerformClick();           //  имитируем нажатие 
                    }
                    break;
                // Горячая клавиша "ОБНОВИТЬ"
                case Keys.F5:
                    if (e.KeyCode == Keys.F5)                       //  F5
                    {
                        refreshToolStripMenuItem.PerformClick();    //  имитируем нажатие     
                    }
                    break;
                default:
                    break;
            }
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            //подключение к БД mysql

            //string connectionString = @"Server=192.168.100.2; Database=zpdb; Uid=root; Pwd=root;";
            //sqlConnection = new SqlConnection(connectionString);  
            

            //подключение к локальной БД Visual
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\Users\User\Desktop\_VS Project\zp\zp\Database1.mdf; Integrated Security = True; MultipleActiveResultSets=True";
            sqlConnection = new SqlConnection(connectionString);

            try
            {
                await sqlConnection.OpenAsync();

                //MessageBox.Show("Подключение прошло успешно!");

                toolStripStatusLabel1.Text = "Количество записей: 0";

                listView1.GridLines = true;
                listView1.FullRowSelect = true;
                listView1.View = View.Details;
                listView1.Columns.Add("№");
                listView1.Columns.Add("Таб. №");
                listView1.Columns.Add("Работник");
                listView1.Columns.Add("Удержание");
                listView1.Columns.Add("Дата 'с'");
                listView1.Columns.Add("Дата 'по'");
                listView1.Columns.Add("Код магазина");
                listView1.Columns.Add("Результат");

                listView2.GridLines = true;
                listView2.FullRowSelect = true;
                listView2.View = View.Details;
                listView2.Columns.Add("№");
                listView2.Columns.Add("Таб. №");
                listView2.Columns.Add("Работник");
                listView2.Columns.Add("Удержание");
                listView2.Columns.Add("Дата 'с'");
                listView2.Columns.Add("Дата 'по'");
                listView2.Columns.Add("Код магазина");
                listView2.Columns.Add("Результат");

                listView3.GridLines = true;
                listView3.FullRowSelect = true;
                listView3.View = View.Details;
                listView3.Columns.Add("№");
                listView3.Columns.Add("Таб. №");
                listView3.Columns.Add("Работник");
                listView3.Columns.Add("Цех");

                await Loadzpworkers1Async();
                autoResizeColumns(listView1);

                await Loadzpworkers2Async();
                autoResizeColumns(listView2);

                await LoadworkersAsync();
                autoResizeColumns(listView3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //авторасширение колонок listView по размеру содержимого
        public static void autoResizeColumns(ListView lv)
        {
            lv.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            ListView.ColumnHeaderCollection cc = lv.Columns;
            for (int i = 0; i < cc.Count; i++)
            {
                int colWidth = TextRenderer.MeasureText(cc[i].Text, lv.Font).Width + 10;
                if (colWidth > cc[i].Width)
                {
                    cc[i].Width = colWidth;
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
        }
        
        //Просмотреть список удержаний за питание и товары в счёт зп
        private async Task Loadzpworkers1Async() 
        {
            SqlDataReader sqlReader = null;

            string query = "SELECT * FROM [zpworkers] WHERE [nzpwh]>='1' AND [nzpwh]<='2' ORDER by Id DESC";

            SqlCommand getzpworkers1Command = new SqlCommand(query, sqlConnection);

            try
            {
                sqlReader = await getzpworkers1Command.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    DateTime thisDate1 = new DateTime();
                    thisDate1 = Convert.ToDateTime(sqlReader["zpd1"]);

                    DateTime thisDate2 = new DateTime();
                    thisDate2 = Convert.ToDateTime(sqlReader["zpd2"]);

                    ListViewItem item = new ListViewItem(new string[] {

                        Convert.ToString(sqlReader["Id"]),
                        Convert.ToString(sqlReader["N"]),
                        Convert.ToString(sqlReader["fio"]),
                        Convert.ToString(sqlReader["zpwh"]),

                        Convert.ToString(thisDate1.ToString("dd.MM.yyyy")),
                        //Convert.ToString(sqlReader["zpd1"]),
                        Convert.ToString(thisDate2.ToString("dd.MM.yyyy")),

                        Convert.ToString(sqlReader["rates"]),
                        Convert.ToString(sqlReader["sum"])

                    });

                    listView1.Items.Add(item);
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                {
                    sqlReader.Close();
                }
            }
        }       

        //Просмотреть список удержаний льготного питания механизаторов
        private async Task Loadzpworkers2Async()
        {
            SqlDataReader sqlReader = null;

            string query = "SELECT * FROM [zpworkers] WHERE [nzpwh]='3' ORDER by Id DESC";

            SqlCommand getzpworkers2Command = new SqlCommand(query, sqlConnection);

            try
            {
                sqlReader = await getzpworkers2Command.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    DateTime thisDate1 = new DateTime();
                    thisDate1 = Convert.ToDateTime(sqlReader["zpd1"]);

                    DateTime thisDate2 = new DateTime();
                    thisDate2 = Convert.ToDateTime(sqlReader["zpd2"]);

                    ListViewItem item = new ListViewItem(new string[] {

                    Convert.ToString(sqlReader["Id"]),
                    Convert.ToString(sqlReader["N"]),
                    Convert.ToString(sqlReader["fio"]),
                    Convert.ToString(sqlReader["zpwh"]),

                    Convert.ToString(thisDate1.ToString("dd.MM.yyyy")),
                    //Convert.ToString(sqlReader["zpd1"]),
                    Convert.ToString(thisDate2.ToString("dd.MM.yyyy")),

                    Convert.ToString(sqlReader["rates"]),
                    Convert.ToString(sqlReader["sum"])
                    });

                    listView2.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                {
                    sqlReader.Close();
                }
            }
        }

        //Просмотреть список работников
        private async Task LoadworkersAsync()
        {
            SqlDataReader sqlReader = null;

            SqlCommand getworkersCommand = new SqlCommand("SELECT * FROM [workers] ORDER by Id DESC", sqlConnection);

            try
            {
                sqlReader = await getworkersCommand.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    ListViewItem item = new ListViewItem(new string[] {

                    Convert.ToString(sqlReader["Id"]),
                    Convert.ToString(sqlReader["N"]),
                    Convert.ToString(sqlReader["fio"]),
                    Convert.ToString(sqlReader["workshop"])
                    });

                    listView3.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                {
                    sqlReader.Close();
                }

            }
        }

        //=========================
        //           МЕНЮ          
        //=========================

        //ДОБАВИТЬ УДЕРЖАНИЕ через Меню
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 add = new Form2(sqlConnection);

            add.StartPosition = FormStartPosition.CenterParent;

            add.ShowDialog();
        }

        //ДОБАВИТЬ +Работник через Меню
        private void newworkerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 addmember = new Form4(sqlConnection);

            addmember.StartPosition = FormStartPosition.CenterParent;

            addmember.ShowDialog();
        }

        //Меню - Выход
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
            Close();
        }

        //=========================//=========================//

        //ИЗМЕНИТЬ УДЕРЖАНИЕ через Меню
        private void modToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count != 0 && listView2.SelectedItems.Count == 0)
            {
                Form3 mod1 = new Form3(sqlConnection, Convert.ToInt32(listView1.SelectedItems[0].SubItems[0].Text));

                mod1.StartPosition = FormStartPosition.CenterParent;

                mod1.ShowDialog();
            }
            else if (listView2.SelectedItems.Count != 0 && listView1.SelectedItems.Count == 0)
            {
                Form3 mod2 = new Form3(sqlConnection, Convert.ToInt32(listView2.SelectedItems[0].SubItems[0].Text));

                mod2.StartPosition = FormStartPosition.CenterParent;

                mod2.ShowDialog();
            }
            else if(listView3.SelectedItems.Count != 0 && listView1.SelectedItems.Count == 0 && listView2.SelectedItems.Count == 0)
            {
                Form5 mod3 = new Form5(sqlConnection, Convert.ToInt32(listView3.SelectedItems[0].SubItems[0].Text));

                mod3.StartPosition = FormStartPosition.CenterParent;

                mod3.ShowDialog();
            }
            else
            {
                MessageBox.Show("Выберите хотя бы один элемент из списка!", "Ошибка!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }


        //ИЗМЕНИТЬ /Работник через Меню
        /*
        private void modToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (listView3.SelectedItems.Count != 0)
            {
                Form5 mod3 = new Form5(sqlConnection, Convert.ToInt32(listView3.SelectedItems[0].SubItems[0].Text));

                mod3.StartPosition = FormStartPosition.CenterParent;

                mod3.ShowDialog();
            }
            else
            {
                MessageBox.Show("Выберите хотя бы один элемент из списка РАБОТНИКОВ!", "Ошибка!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
        */


        //УДАЛИТЬ УДЕРЖАНИЕ через Меню
        private async void deltoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0 && listView2.SelectedItems.Count == 0 && listView3.SelectedItems.Count == 0)
            {
                DialogResult res = MessageBox.Show("Вы действительно хотите удалить эту строку?", "Удаление...", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                switch (res)
                {
                    case DialogResult.OK:

                        SqlCommand delCommand = new SqlCommand("DELETE FROM [zpworkers] WHERE [Id]=@Id", sqlConnection);

                        delCommand.Parameters.AddWithValue("Id", Convert.ToString(listView1.SelectedItems[0].SubItems[0].Text));

                        try
                        {
                            await delCommand.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        // автообновление после удаления
                        refreshToolStripMenuItem.PerformClick();

                        break;
                }
            }
            else if (listView2.SelectedItems.Count > 0 && listView1.SelectedItems.Count == 0 && listView3.SelectedItems.Count == 0)
            {
                DialogResult res = MessageBox.Show("Вы действительно хотите удалить эту строку?", "Удаление...", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                switch (res)
                {
                    case DialogResult.OK:

                        SqlCommand delCommand = new SqlCommand("DELETE FROM [zpworkers] WHERE [Id]=@Id", sqlConnection);

                        delCommand.Parameters.AddWithValue("Id", Convert.ToString(listView2.SelectedItems[0].SubItems[0].Text));

                        try
                        {
                            await delCommand.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        // автообновление после удаления
                        refreshToolStripMenuItem.PerformClick();

                        break;
                }
            }
            else if (listView3.SelectedItems.Count > 0 && listView1.SelectedItems.Count == 0 && listView2.SelectedItems.Count == 0)
            {
                DialogResult res = MessageBox.Show("Вы действительно хотите удалить эту строку?", "Удаление...", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                switch (res)
                {
                    case DialogResult.OK:

                        SqlCommand delCommand = new SqlCommand("DELETE FROM [workers] WHERE [Id]=@Id", sqlConnection);

                        delCommand.Parameters.AddWithValue("Id", Convert.ToString(listView3.SelectedItems[0].SubItems[0].Text));

                        try
                        {
                            await delCommand.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        // автообновление после удаления
                        refreshToolStripMenuItem.PerformClick();

                        break;
                }
            }
            else
            {
                MessageBox.Show("Ни одна строка не была выделена!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //УДАЛИТЬ -Работник через Меню
        private async void deltoolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {
                DialogResult res = MessageBox.Show("Вы действительно хотите удалить эту строку?", "Удаление...", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                switch (res)
                {
                    case DialogResult.OK:

                        SqlCommand delCommand = new SqlCommand("DELETE FROM [workers] WHERE [Id]=@Id", sqlConnection);

                        delCommand.Parameters.AddWithValue("Id", Convert.ToString(listView2.SelectedItems[0].SubItems[0].Text));

                        try
                        {
                            await delCommand.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        // автообновление после удаления
                        refreshToolStripMenuItem.PerformClick();

                        break;
                }
            }
            else
            {
                MessageBox.Show("Ни одна строка не была выделена!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //=========================//

        //ОБНОВИТЬ через Меню
        private async void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();
            listView3.Items.Clear();

            await Loadzpworkers1Async();
            await Loadzpworkers2Async();
            await LoadworkersAsync();

            if (listView1.Visible == true && listView2.Visible == false && listView3.Visible == false)
            {
                toolStripStatusLabel1.Text = "Количество записей: " + listView1.Items.Count;
            }
            else if (listView1.Visible == false && listView2.Visible == true && listView3.Visible == false)
            {
                toolStripStatusLabel1.Text = "Количество записей: " + listView2.Items.Count;
            }
            else if (listView1.Visible == false && listView2.Visible == false && listView3.Visible == true)
            {
                toolStripStatusLabel1.Text = "Количество записей: " + listView3.Items.Count;
            }
        }

        //=========================
        // ПАНЕЛЬ БЫСТРОГО ДОСТУПА 
        //=========================

        //ДОБАВИТЬ УДЕРЖАНИЕ через Панель
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            addwhToolStripMenu1.PerformClick();
        }
        //ДОБАВИТЬ РАБОТНИКА через Панель
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addmemToolStripMenu1.PerformClick();
        }

        //=========================//

        //ИЗМЕНИТЬ через Панель
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            editToolStripMenu1.PerformClick();
        }

        //=========================//

        //УДАЛИТЬ через Панель
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            deltoolStripMenu1.PerformClick();
        }

        //=========================//

        //ОБНОВИТЬ через Панель
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            refreshToolStripMenuItem.PerformClick();
        }

        //=========================//=========================//

        //Показать listBox1 - список удержаний за питание и товары в счёт зп
        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            listView1.Visible = true;
            listView2.Visible = false;
            listView3.Visible = false;

            button1.Visible = false;
            button2.Visible = false;

            toolStripStatusLabel1.Text = "Количество записей: " + listView1.Items.Count;

            //Обновление при открытии списка
            refreshToolStripMenuItem.PerformClick();
        }
        //Показать listBox2 - список удержаний льготного питания механизаторов
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            listView1.Visible = false;
            listView2.Visible = true;
            listView3.Visible = false;

            button1.Visible = false;
            button2.Visible = false;

            toolStripStatusLabel1.Text = "Количество записей: " + listView2.Items.Count;

            //Обновление при открытии списка
            refreshToolStripMenuItem.PerformClick();
        }
        //Показать listBox3 - список работников
        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            listView1.Visible = false;
            listView2.Visible = false;
            listView3.Visible = true;

            button1.Visible = false;
            button2.Visible = false;

            toolStripStatusLabel1.Text = "Количество записей: " + listView3.Items.Count;

            //Обновление при открытии списка
            refreshToolStripMenuItem.PerformClick();
        }

        //=========================//=========================//

        //Закрытие listBox по кнопке1 "X"
        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            if (listView1.Visible == true || listView2.Visible == true || listView3.Visible == true)
            {
                listView1.Visible = false;
                listView2.Visible = false;
                listView3.Visible = false;

                button1.Visible = true;
                button2.Visible = true;

                toolStripStatusLabel1.Text = "Количество записей: 0";
            }
        }
        //Закрытие listBox по кнопке2 "X"
        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            if (listView1.Visible == true || listView2.Visible == true || listView3.Visible == true)
            {
                listView1.Visible = false;
                listView2.Visible = false;
                listView3.Visible = false;

                button1.Visible = true;
                button2.Visible = true;

                toolStripStatusLabel1.Text = "Количество записей: 0";
            }
        }
        //Закрытие listBox по кнопке3 "X"
        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            if (listView1.Visible == true || listView2.Visible == true || listView3.Visible == true)
            {
                listView1.Visible = false;
                listView2.Visible = false;
                listView3.Visible = false;

                button1.Visible = true;
                button2.Visible = true;

                toolStripStatusLabel1.Text = "Количество записей: 0";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form6 add = new Form6(sqlConnection);

            add.StartPosition = FormStartPosition.CenterParent;

            add.ShowDialog();
        }
    }
}
