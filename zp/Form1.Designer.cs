﻿namespace zp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addwhToolStripMenu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addmemToolStripMenu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.deltoolStripMenu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addwhtoolStrip1 = new System.Windows.Forms.ToolStripButton();
            this.edittoolStrip1 = new System.Windows.Forms.ToolStripButton();
            this.deltoolStrip1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addmemtoolStrip1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.reftoolStrip1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.listView3 = new System.Windows.Forms.ListView();
            this.list1toolStrip2 = new System.Windows.Forms.ToolStripButton();
            this.X1toolStrip2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.list2toolStrip2 = new System.Windows.Forms.ToolStripButton();
            this.X2toolStrip2 = new System.Windows.Forms.ToolStripButton();
            this.list3Strip2 = new System.Windows.Forms.ToolStripButton();
            this.X3toolStrip2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(884, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addwhToolStripMenu1,
            this.addmemToolStripMenu1,
            this.toolStripSeparator2,
            this.exitToolStripMenu1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // addwhToolStripMenu1
            // 
            this.addwhToolStripMenu1.Image = ((System.Drawing.Image)(resources.GetObject("addwhToolStripMenu1.Image")));
            this.addwhToolStripMenu1.Name = "addwhToolStripMenu1";
            this.addwhToolStripMenu1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D1)));
            this.addwhToolStripMenu1.Size = new System.Drawing.Size(225, 22);
            this.addwhToolStripMenu1.Text = "Добавить удержание";
            this.addwhToolStripMenu1.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // addmemToolStripMenu1
            // 
            this.addmemToolStripMenu1.Image = ((System.Drawing.Image)(resources.GetObject("addmemToolStripMenu1.Image")));
            this.addmemToolStripMenu1.Name = "addmemToolStripMenu1";
            this.addmemToolStripMenu1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D2)));
            this.addmemToolStripMenu1.Size = new System.Drawing.Size(225, 22);
            this.addmemToolStripMenu1.Text = "Добавить работника";
            this.addmemToolStripMenu1.Click += new System.EventHandler(this.newworkerToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(222, 6);
            // 
            // exitToolStripMenu1
            // 
            this.exitToolStripMenu1.Name = "exitToolStripMenu1";
            this.exitToolStripMenu1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenu1.Size = new System.Drawing.Size(225, 22);
            this.exitToolStripMenu1.Text = "Выход";
            this.exitToolStripMenu1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenu1,
            this.toolStripSeparator4,
            this.deltoolStripMenu1,
            this.toolStripSeparator3,
            this.refreshToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.editToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.editToolStripMenuItem.Text = "Правка";
            // 
            // editToolStripMenu1
            // 
            this.editToolStripMenu1.Image = ((System.Drawing.Image)(resources.GetObject("editToolStripMenu1.Image")));
            this.editToolStripMenu1.Name = "editToolStripMenu1";
            this.editToolStripMenu1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D3)));
            this.editToolStripMenu1.Size = new System.Drawing.Size(164, 22);
            this.editToolStripMenu1.Text = "Изменить";
            this.editToolStripMenu1.Click += new System.EventHandler(this.modToolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(161, 6);
            // 
            // deltoolStripMenu1
            // 
            this.deltoolStripMenu1.Image = ((System.Drawing.Image)(resources.GetObject("deltoolStripMenu1.Image")));
            this.deltoolStripMenu1.Name = "deltoolStripMenu1";
            this.deltoolStripMenu1.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deltoolStripMenu1.Size = new System.Drawing.Size(164, 22);
            this.deltoolStripMenu1.Text = "Удалить";
            this.deltoolStripMenu1.Click += new System.EventHandler(this.deltoolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(161, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("refreshToolStripMenuItem.Image")));
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.refreshToolStripMenuItem.Text = "Обновить";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // addwhtoolStrip1
            // 
            this.addwhtoolStrip1.Image = ((System.Drawing.Image)(resources.GetObject("addwhtoolStrip1.Image")));
            this.addwhtoolStrip1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addwhtoolStrip1.Name = "addwhtoolStrip1";
            this.addwhtoolStrip1.Size = new System.Drawing.Size(142, 22);
            this.addwhtoolStrip1.Text = "Добавить удержание";
            this.addwhtoolStrip1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // edittoolStrip1
            // 
            this.edittoolStrip1.Image = ((System.Drawing.Image)(resources.GetObject("edittoolStrip1.Image")));
            this.edittoolStrip1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.edittoolStrip1.Name = "edittoolStrip1";
            this.edittoolStrip1.Size = new System.Drawing.Size(81, 22);
            this.edittoolStrip1.Text = "Изменить";
            this.edittoolStrip1.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // deltoolStrip1
            // 
            this.deltoolStrip1.Image = ((System.Drawing.Image)(resources.GetObject("deltoolStrip1.Image")));
            this.deltoolStrip1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deltoolStrip1.Name = "deltoolStrip1";
            this.deltoolStrip1.Size = new System.Drawing.Size(71, 22);
            this.deltoolStrip1.Text = "Удалить";
            this.deltoolStrip1.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addwhtoolStrip1,
            this.addmemtoolStrip1,
            this.toolStripSeparator6,
            this.edittoolStrip1,
            this.toolStripSeparator7,
            this.deltoolStrip1,
            this.toolStripSeparator1,
            this.reftoolStrip1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(884, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // addmemtoolStrip1
            // 
            this.addmemtoolStrip1.Image = ((System.Drawing.Image)(resources.GetObject("addmemtoolStrip1.Image")));
            this.addmemtoolStrip1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addmemtoolStrip1.Name = "addmemtoolStrip1";
            this.addmemtoolStrip1.Size = new System.Drawing.Size(140, 22);
            this.addmemtoolStrip1.Text = "Добавить работника";
            this.addmemtoolStrip1.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // reftoolStrip1
            // 
            this.reftoolStrip1.Image = ((System.Drawing.Image)(resources.GetObject("reftoolStrip1.Image")));
            this.reftoolStrip1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.reftoolStrip1.Name = "reftoolStrip1";
            this.reftoolStrip1.Size = new System.Drawing.Size(81, 22);
            this.reftoolStrip1.Text = "Обновить";
            this.reftoolStrip1.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 50);
            this.button1.TabIndex = 7;
            this.button1.Text = "Отчёт за период (удержание)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView1
            // 
            this.listView1.AllowColumnReorder = true;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(0, 74);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(884, 365);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.Visible = false;
            // 
            // listView2
            // 
            this.listView2.AllowColumnReorder = true;
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Location = new System.Drawing.Point(0, 74);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(884, 365);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.Visible = false;
            // 
            // listView3
            // 
            this.listView3.AllowColumnReorder = true;
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.Location = new System.Drawing.Point(0, 74);
            this.listView3.MultiSelect = false;
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(884, 365);
            this.listView3.TabIndex = 5;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.Visible = false;
            // 
            // list1toolStrip2
            // 
            this.list1toolStrip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.list1toolStrip2.Image = ((System.Drawing.Image)(resources.GetObject("list1toolStrip2.Image")));
            this.list1toolStrip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.list1toolStrip2.Name = "list1toolStrip2";
            this.list1toolStrip2.Size = new System.Drawing.Size(162, 22);
            this.list1toolStrip2.Text = "Питание и товары в счёт зп";
            this.list1toolStrip2.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // X1toolStrip2
            // 
            this.X1toolStrip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.X1toolStrip2.Image = ((System.Drawing.Image)(resources.GetObject("X1toolStrip2.Image")));
            this.X1toolStrip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.X1toolStrip2.Name = "X1toolStrip2";
            this.X1toolStrip2.Size = new System.Drawing.Size(23, 22);
            this.X1toolStrip2.Text = "toolStripButton10";
            this.X1toolStrip2.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // list2toolStrip2
            // 
            this.list2toolStrip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.list2toolStrip2.Image = ((System.Drawing.Image)(resources.GetObject("list2toolStrip2.Image")));
            this.list2toolStrip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.list2toolStrip2.Name = "list2toolStrip2";
            this.list2toolStrip2.Size = new System.Drawing.Size(196, 22);
            this.list2toolStrip2.Text = "Льготное питание механизаторов";
            this.list2toolStrip2.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // X2toolStrip2
            // 
            this.X2toolStrip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.X2toolStrip2.Image = ((System.Drawing.Image)(resources.GetObject("X2toolStrip2.Image")));
            this.X2toolStrip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.X2toolStrip2.Name = "X2toolStrip2";
            this.X2toolStrip2.Size = new System.Drawing.Size(23, 22);
            this.X2toolStrip2.Text = "toolStripButton11";
            this.X2toolStrip2.Click += new System.EventHandler(this.toolStripButton11_Click);
            // 
            // list3Strip2
            // 
            this.list3Strip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.list3Strip2.Image = ((System.Drawing.Image)(resources.GetObject("list3Strip2.Image")));
            this.list3Strip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.list3Strip2.Name = "list3Strip2";
            this.list3Strip2.Size = new System.Drawing.Size(120, 22);
            this.list3Strip2.Text = "Список работников";
            this.list3Strip2.Click += new System.EventHandler(this.toolStripButton12_Click);
            // 
            // X3toolStrip2
            // 
            this.X3toolStrip2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.X3toolStrip2.Image = ((System.Drawing.Image)(resources.GetObject("X3toolStrip2.Image")));
            this.X3toolStrip2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.X3toolStrip2.Name = "X3toolStrip2";
            this.X3toolStrip2.Size = new System.Drawing.Size(23, 22);
            this.X3toolStrip2.Text = "toolStripButton13";
            this.X3toolStrip2.Click += new System.EventHandler(this.toolStripButton13_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.list1toolStrip2,
            this.X1toolStrip2,
            this.toolStripSeparator5,
            this.list2toolStrip2,
            this.X2toolStrip2,
            this.toolStripSeparator8,
            this.list3Strip2,
            this.X3toolStrip2});
            this.toolStrip2.Location = new System.Drawing.Point(0, 49);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(884, 25);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip3";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(150, 85);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 50);
            this.button2.TabIndex = 8;
            this.button2.Text = "Отчёт за период (механизаторы)";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView3);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(900, 500);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Удержание за питание";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addwhToolStripMenu1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenu1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenu1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deltoolStripMenu1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton addwhtoolStrip1;
        private System.Windows.Forms.ToolStripButton edittoolStrip1;
        private System.Windows.Forms.ToolStripButton deltoolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton reftoolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ToolStripButton addmemtoolStrip1;
        private System.Windows.Forms.ToolStripMenuItem addmemToolStripMenu1;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ToolStripButton list1toolStrip2;
        private System.Windows.Forms.ToolStripButton X1toolStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton list2toolStrip2;
        private System.Windows.Forms.ToolStripButton X2toolStrip2;
        private System.Windows.Forms.ToolStripButton list3Strip2;
        private System.Windows.Forms.ToolStripButton X3toolStrip2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Button button2;
    }
}

