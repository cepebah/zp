﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zp.Properties;

///
///     ПОДКЛЮЧЕНИЕ К БД
///

namespace zp
{
    class DB
    {
        public MySqlConnectionStringBuilder sqCon;

        public DB()
        {

            sqCon = new MySqlConnectionStringBuilder();
            sqCon.AllowZeroDateTime = true;
            //sqCon.SslMode = MySqlSslMode.None;

            //sqCon.Server = "192.168.100.2";

            sqCon.Server = "192.168.1.99";

            //sqCon.Server = "178.124.172.147";
            sqCon.Database = "zpdb";
            sqCon.UserID = "root";
            sqCon.Password = "root";
            sqCon.Port = 3307;
            sqCon.CharacterSet = "cp1251";

            //sqCon.Server = "178.124.172.147";
            //sqCon.Database = "zpdb";
            //sqCon.UserID = "user";
            //sqCon.Password = "root";
            //sqCon.Port = 3307;
            //sqCon.CharacterSet = "utf8mb4";


        }
        private string isNull(MySqlDataReader dr, string col)
        {
            return dr.IsDBNull(dr.GetOrdinal(col)) ? null : dr.GetString(col);
        }
        public bool Execute(string query, List<MySqlParameter> col)
        {

            using (MySqlConnection con = new MySqlConnection())
            {
                con.ConnectionString = sqCon.ConnectionString;
                MySqlCommand com = new MySqlCommand(query, con);
                com.Parameters.AddRange(col.ToArray());
                try
                {
                    con.Open();
                    var result = com.ExecuteNonQuery();
                    if (result == 1) return true;
                    else return false;
                }
                catch (Exception)
                {
                   
                    return false;
                }
                finally
                {
                    con.Close();
                  
                }
            }
        }

        public MySqlConnection GetConnection()
        {
           return new MySqlConnection(sqCon.ConnectionString);

        }

        public List<string> getRecords(string name)
        {
            string sql = "SELECT * FROM users WHERE name = ?name";
            List<string> ls = null;
            using (MySqlConnection con = new MySqlConnection())
            {
                con.ConnectionString = sqCon.ConnectionString;
                MySqlCommand com = new MySqlCommand(sql, con);
                com.Parameters.AddWithValue("?name", name);

                try
                {
                    con.Open();
                    using (MySqlDataReader dr = com.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            ls = new List<string>();
                            while (dr.Read())
                            {

                                ls.Add(dr.GetString("name"));

                            }
                        }
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
                
            }
            return ls;
        }

        /*
        public List<Group> GetGroups()
        {
            string sql = @"SELECT * FROM groups";
            var param = new List<MySqlParameter>();
            //param.Add(new MySqlParameter("?id", id));
            return sqlGroupList(sql, param);
        }

        public List<Group> GetGroupsWithCfg(bool updated_old = false)
        {
            string sql = "SELECT grp.id, grp.cid, grp.title, grp.desk, grp.username, grp.msg_audio, grp.msg_doc, grp.msg_photo, grp.msg_sticker, " +
                "grp.msg_text, grp.msg_video, grp.msg_voice, grp.rules, grp.pin_msgid, grp.created_at, grp.updated_at, grp.banned," +
                "cfg.id as cfg_id, cfg.autoban_bot, cfg.ban_channel, cfg.biginer_time, cfg.del_msg, cfg.img_stat, cfg.is_rating, cfg.lang, " +
                "cfg.link_del, cfg.max_warn, cfg.more_rat, cfg.name_filter, cfg.rsmile, cfg.set_all_rating, cfg.share_ban, cfg.silent_rating, cfg.thanks_msg, cfg.welcome, " +
                "cfg.beginers, cfg.limits, cfg.del_msg_srv FROM groups as grp, config_groups as cfg WHERE grp.id = cfg.group_id";
            var param = new List<MySqlParameter>();
            //param.Add(new MySqlParameter("?id", id));
            if (updated_old)
            {
                sql += " and updated_at < ?upddate";
                param.Add(new MySqlParameter("?upddate", DateTime.UtcNow.Subtract(TimeSpan.FromDays(45))));
            }
            return sqlGroupWithCfgList(sql, param);
        }

        private Group sqlGroup(string sql, List<MySqlParameter> param)
        {
            using (MySqlConnection con = new MySqlConnection())
            {
                con.ConnectionString = sqCon.ConnectionString;
                MySqlCommand com = new MySqlCommand(sql, con);
                com.Parameters.AddRange(param.ToArray());

                try
                {
                    con.Open();
                    using (MySqlDataReader dr = com.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                var c = new Group();

                                c.Id = dr.GetInt32("id");
                                c.Img = isNull(dr, "img");
                                c.Title = isNull(dr, "title");
                                c.Description = isNull(dr, "desk");
                                c.Username = isNull(dr, "username");
                                c.Rules = isNull(dr, "rules");
                                c.PinMsgId = dr.GetInt32("pin_msgid");
                                c.Cid = dr.GetInt64("cid");
                                c.MsgText = dr.GetInt32("msg_text");
                                c.MsgPhoto = dr.GetInt32("msg_photo");
                                c.MsgVideo = dr.GetInt32("msg_video");
                                c.MsgDoc = dr.GetInt32("msg_doc");
                                c.MsgAudio = dr.GetInt32("msg_audio");
                                c.MsgVoice = dr.GetInt32("msg_voice");
                                c.MsgSticker = dr.GetInt32("msg_sticker");
                                c.CreatedAt = dr.GetDateTime("created_at");
                                c.UpdatedAt = dr.GetDateTime("updated_at");
                                c.Ban = dr.GetBoolean("banned");
                                c.Config = GetGroupConfig(c.Id);

                                return c;


                            }
                        }
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
                finally
                {
                    WrCons(sql);
                }
            }
            return null;
        }
        */
    }
}
