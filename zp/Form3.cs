﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zp
{
    public partial class Form3 : Form
    {
        SqlConnection SqlConnection = null;

        int id;

        public Form3(SqlConnection connection, int id)
        {
            InitializeComponent();

            SqlConnection = connection;

            this.id = id;
        }

        private async void Form3_Load(object sender, EventArgs e)
        {
            SqlCommand getUpdateCommand = new SqlCommand("SELECT [N], [fio], [zpwh], [zpd1], [summa], [rates], [sum] FROM [zpworkers] WHERE [Id]=@id", SqlConnection);

            getUpdateCommand.Parameters.AddWithValue("Id", id);

            SqlDataReader sqlReader = null;

            try
            {
                sqlReader = await getUpdateCommand.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    dateTimePicker1.Value = Convert.ToDateTime(sqlReader["zpd1"]);

                    comboBox1.Text = Convert.ToString(sqlReader["zpwh"]);
                    comboBox2.Text = Convert.ToString(sqlReader["rates"]);
                    textBox1.Text = Convert.ToString(sqlReader["N"]);
                    textBox2.Text = Convert.ToString(sqlReader["fio"]);
                    textBox3.Text = Convert.ToString(sqlReader["sum"]);

                    string s = "Сумма";
                    s = Convert.ToString(sqlReader["summa"]);
                }

                // выделение всего текста в поле
                // для быстрого редактирования
                // после получения фокуса

                textBox1.SelectAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            SqlCommand updateCommand = new SqlCommand("UPDATE [zpworkers] SET [N]=@N, [fio]=@fio, [zpwh]=@zpwh, [zpd1]=@zpd1, [rates]=@rates, [sum]=@sum, [nzpwh]=@nzpwh WHERE [Id]=@Id", SqlConnection);

            //проеобразования формата даты из обычного в обратный для сортировки
            string theDate1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");

            updateCommand.Parameters.AddWithValue("Id", id);
            updateCommand.Parameters.AddWithValue("zpd1", theDate1);
            updateCommand.Parameters.AddWithValue("zpwh", comboBox1.Text);
            updateCommand.Parameters.AddWithValue("rates", comboBox2.Text);
            updateCommand.Parameters.AddWithValue("N", textBox1.Text);
            updateCommand.Parameters.AddWithValue("fio", textBox2.Text);
            updateCommand.Parameters.AddWithValue("sum", Convert.ToDouble(textBox3.Text));

            string s1 = "1";
            string s2 = "2";
            string s3 = "3";

            if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Магазин")
            {
                updateCommand.Parameters.AddWithValue("nzpwh", s1);
            }
            else if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Столовая")
            {
                updateCommand.Parameters.AddWithValue("nzpwh", s2);
            }
            else if (comboBox1.Text == "Льготное питание механизаторов" & comboBox2.Text == "Столовая")
            {
                updateCommand.Parameters.AddWithValue("nzpwh", s3);
            }

            try
            {
                await updateCommand.ExecuteNonQueryAsync();

                MessageBox.Show("Изменения сохранены! \nЧтобы продолжить, нажмите 'ОК'.", "Изменение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox3.Focus();
            }
        }

        private async void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            ///
            /// ИЗМЕНЕНИЕ ДАННЫХ по нажатию Enter
            ///

            if (e.KeyCode == Keys.Enter)
            {
                SqlCommand updateCommand = new SqlCommand("UPDATE [zpworkers] SET [N]=@N, [fio]=@fio, [zpwh]=@zpwh, [zpd1]=@zpd1, [rates]=@rates, [sum]=@sum, [nzpwh]=@nzpwh WHERE [Id]=@Id", SqlConnection);

                //проеобразования формата даты из обычного в обратный для сортировки
                string theDate1 = dateTimePicker1.Value.ToString("yyyy.MM.dd");

                updateCommand.Parameters.AddWithValue("Id", id);
                updateCommand.Parameters.AddWithValue("zpd1", theDate1);
                updateCommand.Parameters.AddWithValue("zpwh", comboBox1.Text);
                updateCommand.Parameters.AddWithValue("rates", comboBox2.Text);
                updateCommand.Parameters.AddWithValue("N", textBox1.Text);
                updateCommand.Parameters.AddWithValue("fio", textBox2.Text);
                updateCommand.Parameters.AddWithValue("sum", Convert.ToDouble(textBox3.Text));

                string s1 = "1";
                string s2 = "2";
                string s3 = "3";

                if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Магазин")
                {
                    updateCommand.Parameters.AddWithValue("nzpwh", s1);
                }
                else if (comboBox1.Text == "За питание и товары в счёт зар. платы" & comboBox2.Text == "Столовая")
                {
                    updateCommand.Parameters.AddWithValue("nzpwh", s2);
                }
                else if (comboBox1.Text == "Льготное питание механизаторов" & comboBox2.Text == "Столовая")
                {
                    updateCommand.Parameters.AddWithValue("nzpwh", s3);
                }

                try
                {
                    await updateCommand.ExecuteNonQueryAsync();

                    MessageBox.Show("Изменения сохранены! \nЧтобы продолжить, нажмите 'ОК'.", "Изменение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
