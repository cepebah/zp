﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zp
{
    public partial class Form4 : Form
    {
        SqlConnection SqlConnection = null;

        public Form4(SqlConnection connection)
        {
            InitializeComponent();

            SqlConnection = connection;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            SqlDataReader sqlReader = null;

            SqlCommand subdivCommand = new SqlCommand("INSERT INTO [workers] (N, fio, workshop)VALUES(@N, @fio, @workshop)", SqlConnection);

            subdivCommand.Parameters.AddWithValue("N", textBox1.Text);
            subdivCommand.Parameters.AddWithValue("fio", textBox2.Text);
            subdivCommand.Parameters.AddWithValue("workshop", comboBox1.Text);

            try
            {
                await subdivCommand.ExecuteNonQueryAsync();

                MessageBox.Show("Работник добавлен! \nЧтобы продолжить, нажмите 'ОК'.", "Изменение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                textBox1.Clear();
                textBox2.Clear();
                comboBox1.Text = "";

                textBox1.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }

        private async void textBox1_TextChanged(object sender, EventArgs e)
        {
            SqlDataReader sqlReader = null;

            SqlCommand getfio = new SqlCommand("SELECT [N], [fio] FROM [workers]", SqlConnection);

            try
            {
                sqlReader = await getfio.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    if (textBox1.Text == sqlReader["N"].ToString())
                    {
                        textBox2.Text = sqlReader["fio"].ToString();
                        MessageBox.Show("Такой работник уже есть!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox1.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && textBox1.Text == "") // проверка на незаполненное поле "Таб.№"
            {
                MessageBox.Show("Не заполнено обязательное поле 'Таб. №'!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.KeyCode == Keys.Enter && textBox1.Text != "")
            {
                if (e.KeyCode == Keys.Enter && textBox2.Text == "") // проверка на отсутствие табельного номера в базе
                {
                    MessageBox.Show("Такого работника не существует!\nНеобходимо добавить нового работника!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Text = "";
                    textBox1.Focus();

                }
                else if (textBox1.Text != "")
                {
                    textBox2.Focus();
                }
            }

            //textBox2.Focus();
        }

        private /*async*/ void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            //SqlDataReader sqlReader = null;

            SqlCommand getfio = new SqlCommand("SELECT [fio] FROM [workers]", SqlConnection);

            if (e.KeyCode == Keys.Enter && textBox2.Text == "")
            {
                MessageBox.Show("Не заполнено обязательное поле 'ФИО работника'!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            /*
            else if (e.KeyCode == Keys.Enter && textBox2.Text == sqlReader["fio"].ToString())
            {
                try
                {
                    sqlReader = await getfio.ExecuteReaderAsync();

                    while (await sqlReader.ReadAsync())
                    {
                        MessageBox.Show("Такой работник уже есть!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        textBox1.Text = "";
                        textBox1.Focus();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sqlReader != null && !sqlReader.IsClosed)
                        sqlReader.Close();
                }
            }

            if (e.KeyCode == Keys.Enter)
            {
                comboBox1.Focus();
            }
            */
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void Form4_Load(object sender, EventArgs e)
        {
            SqlCommand getsubdivCommand = new SqlCommand("SELECT [sub] FROM [subdiv]", SqlConnection);

            SqlDataReader sqlReader = null;

            try
            {
                sqlReader = await getsubdivCommand.ExecuteReaderAsync();

                while (await sqlReader.ReadAsync())
                {
                    comboBox1.Items.Add(sqlReader["sub"]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (sqlReader != null && !sqlReader.IsClosed)
                    sqlReader.Close();
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            string term = comboBox1.Text;
            var result = from v in comboBox1.Items.Cast<string>() where v.IndexOf(term) >= 0 select v;

            //Показываем строки в которых есть введенное вхождение
            foreach (string find in result)
            {
                comboBox1.AutoCompleteCustomSource.Add(find);
            }
        }

        private async void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SqlDataReader sqlReader = null;

                SqlCommand subdivCommand = new SqlCommand("INSERT INTO [workers] (N, fio, workshop)VALUES(@N, @fio, @workshop)", SqlConnection);

                subdivCommand.Parameters.AddWithValue("N", textBox1.Text);
                subdivCommand.Parameters.AddWithValue("fio", textBox2.Text);
                subdivCommand.Parameters.AddWithValue("workshop", Convert.ToString(comboBox1.Text));

                try
                {
                    await subdivCommand.ExecuteNonQueryAsync();

                    MessageBox.Show("Работник добавлен! \nЧтобы продолжить, нажмите 'ОК'.", "Изменение...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    textBox1.Clear();
                    textBox2.Clear();
                    comboBox1.Text = "";

                    textBox1.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (sqlReader != null && !sqlReader.IsClosed)
                        sqlReader.Close();
                }
            }
        }
    }
}
